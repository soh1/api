# sohace

This project holds the API for sohace project.

## Requirements

* Linux (Kernel >= 3.10)
* Docker
* Docker-Compose

## Building the images

To build the needed images just execute the command:

```bash
python build.py
```

## Updating local hosts

To enable the local hosts you should add to yout hosts file the next line:

```
172.17.0.1 sohace.local
```

## Creating local directories

To create all the needed directories execute sh create_directories.sh.

## Starting the containers

To start the container you should execute:

```
docker-compose up -d
```

## Development

To start development after bringing up the cluster node enter on django node with sh enter_django.sh and
execute the script init_database.sh, will prepare all the structure, fixtures files, etc... to start
development.

### Running the tests

* **Unitary tests**: ```python manage.py test```
