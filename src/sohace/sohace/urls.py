# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions

admin.autodiscover()

admin_urls = [
    url(r'^admin/', admin.site.urls)
]

class WelcomeView(APIView):

    permission_classes = (permissions.AllowAny,)

    def get(self, request):
        return Response({
            "name": "sohace",
            "versions": ["v1"]
        })

urlpatterns = admin_urls + [
    url(r'^$', WelcomeView.as_view()),
    url(r'^', include('core.urls')),
]
