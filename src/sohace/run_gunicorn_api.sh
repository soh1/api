#!/usr/bin/env bash
gunicorn --workers=3 --bind 0.0.0.0:3000 sohace.wsgi:application
