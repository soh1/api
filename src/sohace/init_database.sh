#!/bin/bash

echo "Migrating..."
python manage.py migrate

echo "Copying initial media..."
rm -rf /opt/files/*
cp -R /opt/code/fixtures/files/* /opt/files/.

echo "Fixturing..."

# Base
python manage.py loaddata 001_root_users

# Extra scripts
python manage.py postgres_functions
