# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.contrib import admin
from django.contrib import messages
from nested_inline.admin import NestedStackedInline, NestedModelAdmin, NestedTabularInline

