
from django.core.management.base import BaseCommand
from django.db import connection


json2_array_text = """
CREATE OR REPLACE FUNCTION jsonb_arr2text_arr(_js jsonb)
   RETURNS text[] AS
$func$
SELECT ARRAY(SELECT jsonb_array_elements_text(_js))
$func$
LANGUAGE sql IMMUTABLE;
"""

json2_array_int = """
CREATE OR REPLACE FUNCTION jsonb_arr2int_arr(_js jsonb)
   RETURNS integer[] AS
$func$
SELECT array_agg(x)::INTEGER[] FROM jsonb_array_elements_text(_js) x
$func$
LANGUAGE sql IMMUTABLE;
"""

array_min = """
CREATE OR REPLACE FUNCTION arr_min(anyarray) RETURNS integer AS
'SELECT MIN(i) FROM unnest($1) i' LANGUAGE sql IMMUTABLE;
"""

array_max = """
CREATE OR REPLACE FUNCTION arr_max(anyarray) RETURNS integer AS
'SELECT MAX(i) FROM unnest($1) i' LANGUAGE sql IMMUTABLE;
"""

json2array_range = """
CREATE OR REPLACE FUNCTION jsonb_arr2range(_js jsonb)
   RETURNS INT4RANGE AS
$func$
SELECT
  int4range(arr_min(array_agg(x)::INTEGER[]), arr_max(array_agg(x)::INTEGER[]), '[]')
FROM jsonb_array_elements_text(_js) x
$func$
LANGUAGE sql IMMUTABLE;
"""


class Command(BaseCommand):

    help = u'Add PostgreSQL Functiones needed for querying to database'

    def handle(self, *args, **options):

        queries = [
            json2_array_text,
            json2_array_int,
            array_min,
            array_max,
            json2array_range,
        ]

        for query in queries:
            cursor = connection.cursor()
            cursor.execute(query, [])


