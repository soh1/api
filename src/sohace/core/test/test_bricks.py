
from core.service.bricks import BrickWall
from core.base.base_test import BaseTest

class TestBricks(BaseTest):
    """
    python manage.py test core.test.test:bricks.TestBricks
    """

    def test_root(self):
        """
        python manage.py test core.test.test_bricks.TestBricks
        """
        instance = BrickWall()
        wall = [[1,2,2,1],
        [3,1,2],
        [1,3,2],
        [2,4],
        [3,1,2],
        [1,3,1,1]]

        result = instance.cross(wall)

        print(result)



