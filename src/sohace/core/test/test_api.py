# -*- coding: utf-8 -*-

import simplejson
from core.base.base_test import BaseTest
from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient


class TestApi(BaseTest):
    """
    python manage.py test core.test.test_api.TestApi
    """

    def test_root(self):
        """
        python manage.py test core.test.test_api.TestApi.test_root
        """
        client = APIClient(enforce_csrf_checks=True)

        rv = client.get('/films')
        self.assertEquals(200, rv.status_code)

        json = simplejson.loads(rv.content)
        self.assertTrue("title" in json.keys())
        self.assertTrue("opening_crawl" in json.keys())
        self.assertTrue("starships" in json.keys())

        for item in json.get("starships"):
            self.assertTrue(len(item) > 0)
            self.assertFalse("swapi.co" in item)

    def test_startship(self):
        """
        python manage.py test core.test.test_api.TestApi.test_startship
        """
        client = APIClient(enforce_csrf_checks=True)

        rv = client.get('/starship/2')
        self.assertEquals(200, rv.status_code)

        json = simplejson.loads(rv.content)

        self.assertTrue("name" in json.keys())
        self.assertTrue("manufacturer" in json.keys())
        self.assertTrue("cargo_capacity" in json.keys())
        self.assertTrue(len(json["cargo_capacity"]) > 0)
        self.assertTrue(len(json["name"]) > 0)
        self.assertTrue(len(json["manufacturer"]) > 0)
