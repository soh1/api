# -*- coding: utf-8 -*-

import simplejson
from core.base.base_test import BaseTest
from rest_framework.test import APIRequestFactory
from rest_framework.test import APIClient


class TestCore(BaseTest):
    """
    python manage.py test core.test.test_core.TestCore
    """

    def test_root(self):
        """
        python manage.py test core.test.test_core.TestCore.test_root
        """
        client = APIClient(enforce_csrf_checks=True)

        rv = client.get('/')
        self.assertEquals(200, rv.status_code)

        json = simplejson.loads(rv.content)
        self.assertTrue("name" in json.keys())
        self.assertTrue("versions" in json.keys())
