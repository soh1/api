# -*- coding: utf-8 -*-

from django.test import TestCase
from rest_framework.test import APIClient
import simplejson


class BaseTest(TestCase):

    fixtures = [
        '001_root_users',
    ]

