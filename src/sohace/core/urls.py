# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import permissions
from core.service.film import FilmAPI

class FilmsView(APIView):
    permission_classes = (permissions.AllowAny,)

    def transform_endpoint(self, url_list):
        cleaned_list = []

        for item in url_list:
            # TODO: Split base path and protocol to do it right
            item_split = item.split("https://swapi.co/api")
            cleaned_list.append(item_split[1])

        return cleaned_list

    def get(self, request):
        api = FilmAPI()
        # title, opening_crawl, startships
        film = api.fetch_film()
        return Response({
            "title": film.get("title"),
            "opening_crawl": film.get("opening_crawl"),
            "starships": self.transform_endpoint(film.get("starships")),
        })

class StartshipView(APIView):
    permission_classes = (permissions.AllowAny,)

    def get(self, request, pk):
        api = FilmAPI()
        starship = api.fetch_starship(pk)
        # name, manufacturer, cargo_capacity
        return Response({
            "name": starship.get("name"),
            "manufacturer": starship.get("manufacturer"),
            "cargo_capacity": starship.get("cargo_capacity"),
        })

urlpatterns = [
    url(r'^films$', FilmsView.as_view()),
    url(r'^starship/(?P<pk>.+)$', StartshipView.as_view()),
]
