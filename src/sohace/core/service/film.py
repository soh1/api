import requests


class FilmAPI(object):

    def fetch_film(self):
        rv = requests.get('https://swapi.co/api/films/1/')
        json = rv.json()
        return json

    def fetch_starship(self, pk):
        rv = requests.get('https://swapi.co/api/starships/{0}/'.format(pk))
        json = rv.json()
        return json
