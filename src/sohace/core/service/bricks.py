

class BrickWall(object):
    rows = None
    matrix = None
    path_shortest = None

    def __init__(self):
        self.rows = []
        self.matrix = []
        self.path_shortest = None

    def cross(self, input):
        for row in input:
            self.rows.append(row)

        self.draw_matrix()
        self.check_paths()

    def check_paths(self):
        # Get shortest only first row
        index = 0
        for start_point in self.matrix[0]:
            # start point real
            # using index column (real) index
            self.calculate_crossing_bricks(index)
            index += 1

    def calculate_crossing_bricks(self, index):
        total_bricks_crossed = 0

        for i in (1, len(self.matrix)-1):
            row_current = self.matrix[i]
            # calculate if edge or not
            for j in row_current:
                is_edge = self.is_edge(row_current, self.matrix[i-1], j)
                if not is_edge:
                    total_bricks_crossed += 0

    def is_edge(self, row, prev_row, index):
        # get nearer numbers
        left_column = None
        right_column = None
        current_column = row[index]
        if index > 0:
            left_column = row[index -1]
        if index < len(row)-1:
            right_column = row[index+1]

        if not left_column or not right_column:
            return False

        # calculate if the center of the upper brick
        # is on the edge
        brick_id = prev_row[index]
        brick_width = self.calculate_width(prev_row,
                brick_id)
        is_p = bool(brick_width % 2 == 0)
        if not is_p:
            return False

        left_upper = None
        right_upper = None

        if index > 0:
            left_upper = prev_row[index-1]
        if index < len(row)-1:
            right_column = prev_row[index+1]

        if not left_upper or not right_upper:
            return False

        if left_upper != brick_id or right_upper != brick_id:
            return True

        return False

    def calculate_width(self, row, brick_id):
        total = 0
        for item in row:
            if item == brick_id:
                total += 1

        return total

    def draw_matrix(self):
        matrix = []
        for row in self.rows:
            brick_number = 0
            matrix_row = []
            for width in row:
                # Add an edge, but does not have widtho
                brick_number += 1
                for i in range(0, width):
                    matrix_row.append(brick_number) # brick

            matrix.append(matrix_row)

        self.matrix = matrix
