NAME = 'sohace'

VERSION = '0.0.0'

FABRIC_CONFIGURATION = {
    'qa': {
        'root': '/home/projects/sohace',
        'hosts': ['root@gecko-qa'],
        'docker_compose_file': 'docker-compose-qa.yml',
        'migrate': True,
        'initial_database': True
    },
    'prod': {
        'root': '/home/projects/sohace',
        'hosts': ['root@gecko-prod'],
        'docker_compose_file': 'docker-compose-prod.yml',
        'migrate': True,
        'initial_database': False
    },
}
