import os
import time

from fabric.api import run, env
from fabric.colors import green
from project_metadata import FABRIC_CONFIGURATION

"""
Preparing the configuration
"""
deploy_environment = os.getenv('DEPLOY_ENV', None)

if not deploy_environment or not FABRIC_CONFIGURATION.get(deploy_environment, None):
    raise EnvironmentError('Define the DEPLOY_ENV environment variable')
fabric_conf = FABRIC_CONFIGURATION.get(deploy_environment)

# Configuring Fabric
env.hosts = fabric_conf['hosts']
docker_compose_file = fabric_conf['docker_compose_file']
project_root = fabric_conf['root']
migrate = fabric_conf['migrate']
initial_database = fabric_conf['initial_database']

"""
Deploying
"""
def deploy(tag_or_commit):
    """
    Deploy the given tag or commit on the servers
    :param tag_or_commit:
    :return:
    """
    print(green('> Deploying tag %s on %s' % (tag_or_commit, project_root,)))
    run('cd %s && git reset --hard HEAD' % project_root)
    run('cd %s && git fetch origin' % project_root)
    run('cd %s && git checkout %s' % (project_root, tag_or_commit,))

    print(green('> Building new containers on %s' % project_root))
    run('cd %s && docker-compose -f %s stop' % (project_root, build,))

    print(green('> Restarting API on %s' % project_root))
    run('cd %s && docker-compose -f %s stop' % (project_root, docker_compose_file,))
    run('cd %s && docker-compose -f %s rm --force' % (project_root, docker_compose_file,))
    print(green('> Starting API containers on %s' % project_root))
    run('cd %s && docker-compose -f %s up -d' % (project_root, docker_compose_file,))

    if initial_database:
        print("Starting database... [INITIAL MIGRATION ON]")
        time.sleep(5)
        run('cd %s && sh init_database.sh' % (project_root,))

    if migrate:
        time.sleep(5)
        run('docker exec -t sohace_django python manage.py migrate')
